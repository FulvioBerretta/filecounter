package it.prisma.fileCounter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Scanner;

import static it.prisma.fileCounter.Util.*;

public class Main {
    static Path path;
    static File file;
    static boolean isHiddenIncluded = false;

    public static void main(String[] args) throws IOException {



        do{
            Scanner input = new Scanner(System.in);
            System.out.println("Would you like to include also hidden files in the count ?");
            System.out.println("Type yes to confirm or anything else to not include hidden files ");
            isHiddenIncluded = input.nextLine().equals("yes") ? true : false;

            insertPath();
            countingOccurencies(mergeStreams(path));
            System.out.println("Would you like to do another research? ");

        } while(new Scanner(System.in).next().equals("yes"));
    }
}
