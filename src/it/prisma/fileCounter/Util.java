package it.prisma.fileCounter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;




public class Util {
    //Merging the streams all together. Every stream represents a list of files contained in a specific folder
    public  static Stream<File> mergeStreams(Path path) throws IOException {

            return Files.list(path)
                .filter(Objects::nonNull)
                .map(Path::toFile) //noSuchFileException
                .flatMap( elem -> {
                    if (elem.isDirectory()) {
                        try {
                            return Stream.concat(Stream.of(elem), mergeStreams(Paths.get(elem.getAbsolutePath())));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else return Stream.of(elem);

                    return null;
                });
    }

    //Getting the Extension of a specified File
    public static String getExtension(File file){

    boolean includeHidden = Main.isHiddenIncluded;
    String extension = file.toString().trim().substring(file.toString().trim().lastIndexOf(".") +1);

        if(file.isFile()){
            if(file.getName().contains(".") && !file.isHidden()){
                return extension;
            } else if(file.isHidden() && includeHidden){
                return "hidden file" ;
            } else{
                return "noExtension";
            }

        }else {
            return (file.isHidden() && includeHidden)?  "hidden folder " : "folder";

        }
    }

    //Getting the List of Files and Folders and keeping them in a List
    public static void countingOccurencies(Stream<File> myStream) throws IOException {
        Map<String, Long> countPerLevel = new HashMap<>();
            myStream
            .map(Util::getExtension)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            .forEach(countPerLevel::put);

        countPerLevel.forEach((s, aLong) -> System.out.println(aLong + " " + s));
    }

    public static void insertPath(){
        try{
            System.out.println("Insert a path");
            System.out.println();
            Main.path = Paths.get(new Scanner(System.in).nextLine());
            if (Main.path.toFile().canRead() && Main.path.toFile().isDirectory()){
                Main.file = new File(Main.path.toString());
            } else{
                System.out.println("The given path is not valid");
                insertPath();
            }

        } catch(InvalidPathException e){
            System.out.println("The given path is not valid....");
            insertPath();
        }

    }
}
